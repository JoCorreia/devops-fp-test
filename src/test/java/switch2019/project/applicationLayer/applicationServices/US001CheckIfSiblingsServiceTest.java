package switch2019.project.applicationLayer.applicationServices;


import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.dtoLayer.dtos.BooleanDTO;
import switch2019.project.dtoLayer.dtos.CheckIfSiblingsDTO;
import switch2019.project.dtoLayer.dtosAssemblers.BooleanDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Ala Matos
 */


class US001CheckIfSiblingsServiceTest extends AbstractTest {

    @Mock
    private IPersonRepository personRepository;
    private US001CheckIfSiblingsService us001CheckIfSiblingsService;
    private CheckIfSiblingsDTO checkIfSiblingsDTO;

    @Test
    @DisplayName("Test Controller_US01 - Paulo | Helder | siblings")
    void controller_US01() {

//      Instantiating an us001CheckIfSiblingsService with personRepository as parameter
        us001CheckIfSiblingsService = new US001CheckIfSiblingsService(personRepository);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        Person paulo = Person.createPerson(pauloEmail, pauloName, pauloBirthdateLD, pauloBirthplace);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        Person helder = Person.createPerson(helderEmail, helderName, helderBirthdateLD, helderBirthplace);

//        Arrange people ID
        PersonID pauloID = PersonID.createPersonID(pauloEmail);
        PersonID helderID = PersonID.createPersonID(helderEmail);

//        Add Helder to Paulo list of siblings
        paulo.addSibling(helderID);

//        Act
//          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(pauloID))
                .thenReturn(Optional.of(paulo));

//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(helderID))
                .thenReturn(Optional.of(helder));

//        Expected BooleanDTO
        BooleanDTO expectedBooleanDTO = BooleanDTOAssembler.createDTOFromPrimitiveTypes(true, US001CheckIfSiblingsService.SUCCESS);

//        Act DTO
        checkIfSiblingsDTO = new CheckIfSiblingsDTO(pauloEmail, helderEmail);

//       Expected message
        String message = "Siblings";

//        Assert
        BooleanDTO verifyIfSibling = us001CheckIfSiblingsService.checkIfSiblings(checkIfSiblingsDTO);

//        Assert response
        assertEquals(expectedBooleanDTO, verifyIfSibling);
//        Assert the message
        assertEquals(message, verifyIfSibling.getMsg());
    }


    @Test
    @DisplayName("Test Controller_US01 - Paulo | Helder | Not siblings")
    void controller_US01NotSiblings() throws InvalidArgumentsBusinessException {


//      Instantiating an us001CheckIfSiblingsService with personRepository as parameter
        us001CheckIfSiblingsService = new US001CheckIfSiblingsService(personRepository);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        Person paulo = Person.createPerson(pauloEmail, pauloName, pauloBirthdateLD, pauloBirthplace);

//        Arrange person Helder
        String helderEmail = "helder@gmail.com";
        String helderName = "Helder Fontes";
        LocalDate helderBirthdateLD = LocalDate.of(1983, 1, 30);
        String helderBirthplace = "Vila Nova de Gaia";
        Person helder = Person.createPerson(helderEmail, helderName, helderBirthdateLD, helderBirthplace);

//        Arrange people ID
        PersonID pauloID = PersonID.createPersonID(pauloEmail);
        PersonID helderID = PersonID.createPersonID(helderEmail);

//        Act
//          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(pauloID))
                .thenReturn(Optional.of(paulo));

//        Returning an Optional<Person> helder
        Mockito.when(personRepository.findById(helderID))
                .thenReturn(Optional.of(helder));

//        Act DTO
        checkIfSiblingsDTO = new CheckIfSiblingsDTO(pauloEmail, helderEmail);

//        Expected message
        String expectedMessage = "Not Siblings";

        //Act expected object
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us001CheckIfSiblingsService.checkIfSiblings(checkIfSiblingsDTO));

//        Assert
        assertEquals(expectedMessage, thrown.getMessage());

    }


    @Test
    @DisplayName("Test Controller_US01 - bataguas | bataguas is null")
    void controller_US01NotSiblingsNullPerson() throws InvalidArgumentsBusinessException {

        //      Instantiating an us001CheckIfSiblingsService with personRepository as parameter
        us001CheckIfSiblingsService = new US001CheckIfSiblingsService(personRepository);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        Person paulo = Person.createPerson(pauloEmail, pauloName, pauloBirthdateLD, pauloBirthplace);

//        Arrange bataguas Email
        String bataguasEmail = "bataguas@gmail.com";

//        Arrange people ID
        PersonID pauloID = PersonID.createPersonID(pauloEmail);
        PersonID bataguasID = PersonID.createPersonID(bataguasEmail);

//        Act
//          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(pauloID))
                .thenReturn(Optional.of(paulo));

//        Returning an Optional<Person> bataguas
        Mockito.when(personRepository.findById(bataguasID))
                .thenThrow(new NotFoundArgumentsBusinessException("Second Person does not exist"));

//        Expected message
        String expectedMessage = "Second Person does not exist";


        //Act expected object
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> personRepository.findById(bataguasID));

//        Assert
        assertEquals(expectedMessage, thrown.getMessage());

    }


    @Test
    @DisplayName("Test Controller_US01 - Wolverine | bataguas is null")
    void controller_US01NotSiblingsNullSibling() throws InvalidArgumentsBusinessException {

        //      Instantiating an us001CheckIfSiblingsService with personRepository as parameter
        us001CheckIfSiblingsService = new US001CheckIfSiblingsService(personRepository);

//        Arrange person Paulo
        String pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdateLD = LocalDate.of(1993, 3, 15);
        String pauloBirthplace = "Vila Nova de Gaia";
        Person paulo = Person.createPerson(pauloEmail, pauloName, pauloBirthdateLD, pauloBirthplace);

//        Arrange bataguas Email
        String bataguasEmail = "bataguas@gmail.com";

//        Arrange people ID
        PersonID pauloID = PersonID.createPersonID(pauloEmail);
        PersonID bataguasID = PersonID.createPersonID(bataguasEmail);

//        Act
//          Mock the behaviour of personRepository
//        Returning an Optional<Person> paulo
        Mockito.when(personRepository.findById(bataguasID))
                .thenThrow(new NotFoundArgumentsBusinessException("First Person does not exist"));

//        Returning an Optional<Person> Paulo
        Mockito.when(personRepository.findById(pauloID))
                .thenReturn(Optional.of(paulo));

//        Expected message
        String expectedMessage = "First Person does not exist";

        //Act expected object
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> personRepository.findById(bataguasID));

//        Assert
        assertEquals(expectedMessage, thrown.getMessage());

    }


}


