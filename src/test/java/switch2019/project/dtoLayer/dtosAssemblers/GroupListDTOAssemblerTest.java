package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.GroupListDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GroupListDTOAssemblerTest {

    @Test
    @DisplayName("GroupListDTOAssembler - Test create GroupListDTO from domain objects")
    void groupListDTOAssembler_createDTOFromDomainObject() {

        //Arrange
        String groupDenomination = "Sunday Runners";
        String groupDescription = "All members from Sunday Runners group";
        String groupDateOfCreation = LocalDate.now().toString();

        GroupDTO groupDTO = new GroupDTO(groupDenomination, groupDescription, groupDateOfCreation);
        List<GroupDTO> groupDTOList = new ArrayList<>();
        groupDTOList.add(groupDTO);

        //Act

        GroupListDTOAssembler groupListDTOAssembler = new GroupListDTOAssembler();
        GroupListDTO groupListDTO = groupListDTOAssembler.createDTOFromDomainObject(groupDTOList);

        //Assert
        assertEquals(groupDTOList, groupListDTO.getGroups());
    }

}