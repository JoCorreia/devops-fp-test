package switch2019.project.controllerLayer.unitTests.controllersCLI;


class US017GetPersonBalanceWithinAPeriodControllerTest {
/*
    // US017 Como utilizador, quero obter o saldo do conjunto dos meus movimentos num dado período.

    @Test
    @DisplayName("Get amount between two dates | Happy case")
    void getPersonAmountBetweenTwoDates() {

        // Arrange Person
        Address address = new Address("Rua das Flores", "190", "4415", "Porto", "Portugal");
        Person personJohn = new Person("John", LocalDate.of(1993, 03, 15), null, null, "Porto", address);

        // Arrange Credit Account
        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription,creditAccountDenomination);

        // Arrange Debit Account
        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription,debitAccountDenomination);

        // Arrange of category
        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange of Transaction

        //Transaction A
        String transactionTypeA = "Credit";
        String transactionDescriptionA = "Dez 2019 salary";
        double transactionAmountA = 100.00;
        LocalDate dateA = LocalDate.of(2019, 01, 19);

        // Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Christmas gifts";
        double transactionAmountB = -25.00;
        LocalDate dateB = LocalDate.of(2020, 01, 22);

        // Transaction C
        String transactionTypeC = "Credit";
        String transactionDescriptionC = "Birthday gift";
        double transactionAmountC = 150.00;
        LocalDate dateC = LocalDate.of(2020, 01, 24);

        //Arrange Transactions
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeA, transactionDescriptionA ,transactionAmountA, dateA, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountDebit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeC, transactionDescriptionC ,transactionAmountC, dateC, accountDebit, accountCredit);


        // Arrange Expected Amount

        double expectedAmount = 125.00;

        Controller_US017 controller = new Controller_US017();

        // Act

        double searchAmount = controller.getPersonAmountBetweenTwoDates(personJohn, LocalDate.of(2020, 01, 21), LocalDate.of(2020, 01, 25));

        // Assert
        assertEquals(expectedAmount, searchAmount);
    }


    @Test
    @DisplayName("Get amount between two dates with Dates inverted | Happy case")
    void getPersonAmountBetweenTwoDatesInverted() {

        // Arrange Person
        Address address = new Address("Rua das Flores", "190", "4415", "Porto", "Portugal");
        Person personJohn = new Person("John", LocalDate.of(1993, 03, 15), null, null, "Porto", address);

        // Arrange Credit Account
        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription,creditAccountDenomination);

        // Arrange Debit Account
        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription,debitAccountDenomination);

        // Arrange of category
        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange of Transaction

        //Transaction A
        String transactionTypeA = "Credit";
        String transactionDescriptionA = "Dez 2019 salary";
        double transactionAmountA = 100.00;
        LocalDate dateA = LocalDate.of(2019, 01, 19);

        // Transaction B
        String transactionTypeB = "Debit";
        String transactionDescriptionB = "Christmas gifts";
        double transactionAmountB = -25.00;
        LocalDate dateB = LocalDate.of(2020, 01, 22);

        // Transaction C
        String transactionTypeC = "Credit";
        String transactionDescriptionC = "Birthday gift";
        double transactionAmountC = 150.00;
        LocalDate dateC = LocalDate.of(2020, 01, 24);

        // Arrange Transactions
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeA, transactionDescriptionA ,transactionAmountA, dateA, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountDebit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeC, transactionDescriptionC ,transactionAmountC, dateC, accountDebit, accountCredit);


        // Arrange Expected Amount

        double expectedAmount = 125.00;

        Controller_US017 controller = new Controller_US017();

        // Act

        double searchAmount = controller.getPersonAmountBetweenTwoDates(personJohn, LocalDate.of(2020, 01, 25), LocalDate.of(2020, 01, 21));

        // Assert
        assertEquals(expectedAmount, searchAmount);
    }
*/
}
