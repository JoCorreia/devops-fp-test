package switch2019.project.controllerLayer.unitTests.controllersCLI;

class US020CreateScheduleControllerTest {
/*
    //GROUP
    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Success Case")
    void group_createScheduling() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");

        //Assert
        assertEquals(true, result);
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(1, groupFriends.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Create Two Schedules")
    void group_createSchedulingTwice() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1



        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group runners = new Group(runnersID, personMiguel, runnersDescription, runnersDate);


        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result1 = controller_us020.createGroupTransactionScheduling(runners, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        boolean result2 = controller_us020.createGroupTransactionScheduling(runners, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly");

        //Assert
        assertEquals(true, result1);
        assertEquals(true, result2);
        assertEquals(true, runners.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(true, runners.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly")));
        assertEquals(2, runners.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Create Same Two Schedules")
    void group_createSameSchedulingTwice() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result1 = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        boolean result2 = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");

        //Assert
        assertEquals(true, result1);
        assertEquals(false, result2);
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(1, groupFriends.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Group - Person Not In The Group")
    void group_createGroupTransactionScheduling_PersoNotInTheGroup() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Person Nr.2
        Person personjohn = new Person("John", LocalDate.of(2000, 05, 25));

        //Arrange Group Nr.1

        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> controller_us020.createGroupTransactionScheduling(groupFriends, personjohn, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily"));

        //Assert
        assertEquals(thrown.getMessage(), "Person doesn't belong to the group");
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Workings Days - Starting On Monday - 8 Transactions")
    public void group_schedulingWorkingDaysStartingOnMonday() throws InterruptedException {
        //Day Unit 100ms
        //Monday - Tuesday - Wednesday - Thursday - Friday - x - x - Monday - Tuesday - Wednesday = 8

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);
        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        long timeToSleep = (ChronoUnit.DAYS.between(LocalDate.now(), triggerDate) * 200) + 1900;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Working Days");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, groupFriends.schedulingListSize());
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Working Days")));
        assertEquals(8, groupFriends.getLedger().getRecords().size());
    }

    /*@Test
    @DisplayName("Test For createGroupTransactionScheduling() - Daily - Starting Today - 10 Transactions")
    public void group_schedulingDailyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1

        Denomination runnersDenomination = new Denomination("Runners");
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersDenomination, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 1950;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, groupFriends.schedulingListSize());
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(10, groupFriends.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Weekly - Starting Today - 2 Transactions")
    public void group_schedulingWeeklyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 2150;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, groupFriends.schedulingListSize());
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly")));
        assertEquals(2, groupFriends.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Monthly - Starting Today - 2 Transactions")
    public void group_schedulingMonthlyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);
        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 6300;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, groupFriends.schedulingListSize());
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly")));
        assertEquals(2, groupFriends.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createGroupTransactionScheduling() - Monthly - Starting 5 Days From Now - 1 Transaction")
    public void group_schedulingMonthlyStartingFiveDaysFromNow() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Group Nr.1
        Denomination runnersDenomination = new Denomination("Runners");
        GroupID runnersID = new GroupID(runnersDenomination);
        Description runnersDescription = new Description("Friends");
        DateOfCreation runnersDate = new DateOfCreation(LocalDate.of(2020, 01, 02));

        Group groupFriends = new Group(runnersID, personMiguel, runnersDescription, runnersDate);

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now().plusDays(5);
        long timeToSleep = 6100;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createGroupTransactionScheduling(groupFriends, personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, groupFriends.schedulingListSize());
        assertEquals(true, groupFriends.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly")));
        assertEquals(1, groupFriends.getLedger().getRecords().size());
    }


    //PERSON

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Success Case")
    void person_createScheduling() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");

        //Assert
        assertEquals(true, result);
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(1, personMiguel.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Create Two Schedules")
    void person_createSchedulingTwice() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);
        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result1 = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        boolean result2 = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly");

        //Assert
        assertEquals(true, result1);
        assertEquals(true, result2);
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly")));
        assertEquals(2, personMiguel.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Create Same Two Schedules")
    void person_createSameSchedulingTwice() {
        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Transaction
        LocalDate triggerDate = LocalDate.now();

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result1 = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        boolean result2 = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");

        //Assert
        assertEquals(true, result1);
        assertEquals(false, result2);
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(1, personMiguel.schedulingListSize());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Workings Days - Starting On Monday - 8 Transactions")
    public void person_schedulingWorkingDaysStartingOnMonday() throws InterruptedException {
        //Day Unit 100ms
        //Monday - Tuesday - Wednesday - Thursday - Friday - x - x - Monday - Tuesday - Wednesday = 8

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        long timeToSleep = (ChronoUnit.DAYS.between(LocalDate.now(), triggerDate) * 200) + 1900;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();

        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Working Days");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, personMiguel.schedulingListSize());
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Working Days")));
        assertEquals(8, personMiguel.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Daily - Starting Today - 10 Transactions")
    public void prson_schedulingDailyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 1950;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily");
        Thread.sleep(timeToSleep);


        //Assert
        assertEquals(true, result);
        assertEquals(1, personMiguel.schedulingListSize());
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Daily")));
        assertEquals(10, personMiguel.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Weekly - Starting Today - 2 Transactions")
    public void person_schedulingWeeklyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 2150;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, personMiguel.schedulingListSize());
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Weekly")));
        assertEquals(2, personMiguel.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Monthly - Starting Today - 2 Transactions")
    public void person_schedulingMonthlyStartingToday() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description( "EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description( "John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now();
        long timeToSleep = 6300;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, personMiguel.schedulingListSize());
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly")));
        assertEquals(2, personMiguel.getLedger().getRecords().size());
    }

    @Test
    @DisplayName("Test For createPersonTransactionScheduling() - Monthly - Starting 5 Days From Now - 1 Transaction")
    public void person_schedulingMonthlyStartingFiveDaysFromNow() throws InterruptedException {
        //Day Unit 100ms
        //1s = 10 Transactions

        //Arrange Person Nr.1
        Person personMiguel = new Person("Miguel", LocalDate.of(2000, 10, 25));

        //Arrange Account Nr.1

        Description debAccountDescription = new Description("EmployerSA Account");
        Denomination debAccountDenomination = new Denomination("EmployerSA");
        Account accountBanco = new Account(debAccountDescription, debAccountDenomination);


        //Arrange Account Nr.2

        Description debAccountDescriptionA = new Description("John Allowance");
        Denomination debAccountDenominationA = new Denomination("Allowance Money");
        Account accountJohn = new Account(debAccountDescriptionA, debAccountDenominationA);

        //Arrange Category Nr.1
        Category category = new Category("Allowance");

        //Arrange Triggers/Timers
        LocalDate triggerDate = LocalDate.now().plusDays(5);
        long timeToSleep = 6100;

        //Arrange Controller
        Controller_US020 controller_us020 = new Controller_US020();


        //Act
        boolean result = controller_us020.createPersonTransactionScheduling(personMiguel, "Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly");
        Thread.sleep(timeToSleep);

        //Assert
        assertEquals(true, result);
        assertEquals(1, personMiguel.schedulingListSize());
        assertEquals(true, personMiguel.hasScheduling(new Scheduling("Credit", 50.0, accountBanco, accountJohn, category, "Allowance", triggerDate, "Monthly")));
        assertEquals(1, personMiguel.getLedger().getRecords().size());
    }
    */
}