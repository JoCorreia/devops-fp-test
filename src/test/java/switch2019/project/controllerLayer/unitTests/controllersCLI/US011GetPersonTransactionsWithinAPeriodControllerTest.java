package switch2019.project.controllerLayer.unitTests.controllersCLI;

class US011GetPersonTransactionsWithinAPeriodControllerTest {
/*
    //US011 Como utilizador, quero obter os meus movimentos num dado período.

    @Test
    @DisplayName("Get Records between two dates | Happy case")
    void getPersonRecordsBetweenTwoDatesHappyCase() {

        //Arrange controller

        Controller_US011 controller_us011 = new Controller_US011();

        //Arrange Person

        Person personJohn = new Person("John", LocalDate.of(1993, 7, 8));

        //Arrange of category

        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);


        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //Transaction B
        String transactionTypeB = "Credit";
        String transactionDescriptionB = "Jan 2020 Salary";
        double transactionAmountB = 1500.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);


        //Act

        personJohn.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Act
        ArrayList<Transaction> expectedRecords = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRecords.add(expectedTransaction1);
        expectedRecords.add(expectedTransaction2);

        List<Transaction> result = controller_us011.getPersonRecordsBetweenTwoDates(personJohn, LocalDate.of(2019, 12, 23), LocalDate.of(2020, 02, 23));

        //Assert

        assertEquals(result, expectedRecords);
    }

    @Test
    @DisplayName("Get Records between two dates, with one date off the defined time period | Happy case")
    void getPersonRecordsBetweenTwoDatesDateOffPeriod() {

        //Arrange controller

        Controller_US011 controller_us011 = new Controller_US011();

        //Arrange Person

        Person personJohn = new Person("John", LocalDate.of(1993, 7, 8));

        //Arrange of category

        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);


        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //TransactionB
        String transactionTypeB = "Credit";
        String transactionDescriptionB = "Jan 2020 Salary";
        double transactionAmountB = 1500.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

        //TransactionC
        String transactionTypeC = "Credit";
        String transactionDescriptionC = "Jan 2020 Salary";
        double transactionAmountC = 1500.00;
        LocalDate dateC = LocalDate.of(2018, 01, 01);


        //Act

        personJohn.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);

        //Act
        ArrayList<Transaction> expectedRecords = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRecords.add(expectedTransaction1);
        expectedRecords.add(expectedTransaction2);

        List<Transaction> result = controller_us011.getPersonRecordsBetweenTwoDates(personJohn, LocalDate.of(2019, 12, 23), LocalDate.of(2020, 02, 23));

        //Assert

        assertEquals(result, expectedRecords);
    }

    @Test
    @DisplayName("Get Records between two dates with dates inverted | Happy case")
    void getPersonRecordsBetweenTwoDatesInverted() {

        //Arrange controller

        Controller_US011 controller_us011 = new Controller_US011();

        //Arrange Person

        Person personJohn = new Person("John", LocalDate.of(1993, 7, 8));

        //Arrange of category

        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);


        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //TransactionB
        String transactionTypeB = "Credit";
        String transactionDescriptionB = "Jan 2020 Salary";
        double transactionAmountB = 1500.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);


        //Act

        personJohn.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);

        //Act
        ArrayList<Transaction> expectedRecords = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRecords.add(expectedTransaction1);
        expectedRecords.add(expectedTransaction2);

        List<Transaction> result = controller_us011.getPersonRecordsBetweenTwoDates(personJohn, LocalDate.of(2020, 02, 23), LocalDate.of(2019, 10, 23));

        //Assert

        assertEquals(result, expectedRecords);
    }

    @Test
    @DisplayName("Get Records between two dates | Sad case")
    void getPersonRecordsBetweenTwoDatesSadCase() {

        //Arrange controller

        Controller_US011 controller_us011 = new Controller_US011();

        //Arrange Person

        Person personJohn = new Person("John", LocalDate.of(1993, 7, 8));

        //Arrange of category

        String categoryDenomination = "Salary";
        Category categorySalary = new Category(categoryDenomination);

        //Arrange Credit Account

        Description creditAccountDescription = new Description("John Salary Account");
        Denomination creditAccountDenomination = new Denomination("EmployerSA");

        Account accountCredit = new Account(creditAccountDescription, creditAccountDenomination);

        //Arrange Debit Account

        Description debitAccountDescription = new Description("EmployerSA Account");
        Denomination debitAccountDenomination = new Denomination("EmployerSA");

        Account accountDebit = new Account(debitAccountDescription, debitAccountDenomination);


        //Arrange of Transaction

        //Transaction A
        String transactionType = "Credit";
        String transactionDescription = "Dez 2019 Salary";
        double transactionAmount = 1000.00;
        LocalDate date = LocalDate.of(2019, 12, 25);

        //TransactionB
        String transactionTypeB = "Credit";
        String transactionDescriptionB = "Jan 2020 Salary";
        double transactionAmountB = 1500.00;
        LocalDate dateB = LocalDate.of(2020, 01, 01);

        //TransactionC
        String transactionTypeC = "Credit";
        String transactionDescriptionC = "Jan 2020 Salary";
        double transactionAmountC = 1500.00;
        LocalDate dateC = LocalDate.of(2020, 01, 01);


        //Act

        personJohn.createAndAddTransactionWithDate(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        personJohn.createAndAddTransactionWithDate(categorySalary, transactionTypeC, transactionDescriptionC, transactionAmountC, dateC, accountDebit, accountCredit);

        //Act
        ArrayList<Transaction> expectedRecords = new ArrayList<>();
        Transaction expectedTransaction1 = new Transaction(categorySalary, transactionType, transactionDescription, transactionAmount, date, accountDebit, accountCredit);
        Transaction expectedTransaction2 = new Transaction(categorySalary, transactionTypeB, transactionDescriptionB, transactionAmountB, dateB, accountDebit, accountCredit);
        expectedRecords.add(expectedTransaction1);
        expectedRecords.add(expectedTransaction2);

        List<Transaction> result = controller_us011.getPersonRecordsBetweenTwoDates(personJohn, LocalDate.of(2019, 12, 23), LocalDate.of(2020, 02, 23));

        //Assert

        assertNotEquals(result, expectedRecords);
    }

    @Test
    @DisplayName("Get Records between 2 date - Person not created | Sad Case")
    public void testGetRecordsBetween2Dates_PersonNotCreated() {

        //Arrange controller

        Controller_US011 controller_us011 = new Controller_US011();

        //Arrange of Category

        // no need because there isn't any transaction

        //Arrange of Transaction

        // no transactions

        //Arrange of Persons
        Person personHenrique = null;

        //Act

        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> controller_us011.getPersonRecordsBetweenTwoDates(personHenrique, LocalDate.of(2019,1,26),LocalDate.of(2020, 02, 23)));

        //Assert

        assertEquals(thrown.getMessage(), "Person not created");
    }


 */
}