package switch2019.project.dtoLayer.dtos;

import java.util.Objects;

public class GroupMemberClearanceDTO {

    private String memberID;
    private String clearance;

    public GroupMemberClearanceDTO(String memberID, String clearance) {
        this.memberID = memberID;
        this.clearance = clearance;
    }

    public GroupMemberClearanceDTO() {

    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public String getMemberID() {
        return memberID;
    }

    public String getClearance() {
        return clearance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupMemberClearanceDTO)) return false;
        GroupMemberClearanceDTO that = (GroupMemberClearanceDTO) o;
        return Objects.equals(memberID, that.memberID) &&
                Objects.equals(clearance, that.clearance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberID, clearance);
    }
}
