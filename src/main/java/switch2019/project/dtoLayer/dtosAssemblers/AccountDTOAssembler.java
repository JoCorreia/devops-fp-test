package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.AccountDTO;

public class AccountDTOAssembler {

    public static AccountDTO createDTOFromPrimitiveTypes(String denomination, String description) {

        AccountDTO accountsDTO = new AccountDTO(denomination, description);

        return accountsDTO;
    }
}
