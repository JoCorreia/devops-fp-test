package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.GroupMemberClearanceDTO;

public class GroupMemberClearanceDTOAssembler {

    public static GroupMemberClearanceDTO createDTOFromDomainObject(String memberID, String clearance) {

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(memberID, clearance);
        return groupMemberClearanceDTO;
    }

}
