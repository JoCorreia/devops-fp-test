package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US004GroupsThatAreFamilyService;
import switch2019.project.dtoLayer.dtos.GroupsThatAreFamilyDTO;

/**
 * The type Us 004 groups that are family controller.
 */

@Controller
public class US004GroupsThatAreFamilyController {

    @Autowired
    private US004GroupsThatAreFamilyService us004GroupsThatAreFamilyService;

    /**
     * Instantiates a new Us 004 groups that are family controller.
     *
     * @param us004GroupsThatAreFamilyService the us 004 groups that are family service
     */
    public US004GroupsThatAreFamilyController(US004GroupsThatAreFamilyService us004GroupsThatAreFamilyService) {
        this.us004GroupsThatAreFamilyService = us004GroupsThatAreFamilyService;
    }

    // Como gestor do sistema quero saber quais são os grupos que são família

    /**
     * Gets groups that are family.
     *
     * @return the groups that are family
     */
    public GroupsThatAreFamilyDTO getGroupsThatAreFamily() {
        return us004GroupsThatAreFamilyService.groupsThatAreFamily();
    }
}


