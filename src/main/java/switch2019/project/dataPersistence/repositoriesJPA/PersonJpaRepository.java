package switch2019.project.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.dataModelLayer.dataModel.PersonJpa;

import java.util.List;
import java.util.Optional;

public interface PersonJpaRepository extends CrudRepository<PersonJpa, PersonID> {

	List<PersonJpa> findAll();

	Optional<PersonJpa> findById(PersonID id);

	boolean existsById(PersonID id);

	long count();

	void delete(PersonJpa personJpa);
}